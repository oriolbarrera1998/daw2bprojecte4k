// Creem la instancia del mapa
var chart = am4core.create("chartdiv", am4maps.MapChart);
var x;
// Configurem la definició del mapa a través de la propietat geodata
chart.geodata = am4geodata_worldLow;

// Projectem el mapa
chart.projection = new am4maps.projections.Miller();

// Les zones del mapa (països, estats, etc) són representades per objectes MapPolygonSeries. En el nostre cas són els països de tot el món
var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

// Carreguem els objectes polygon desde GeoJSON
polygonSeries.useGeodata = true;

// Configurem la apariència i comportament dels objectes a través de templates
var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.tooltipText = "{name} {radio}";
polygonTemplate.fill = am4core.color("#74B266");

// Creem el comportament dels objectes del mapa quan hi passem el ratolí per sobre (hover) i definim un fill color alternatiu
var hs = polygonTemplate.states.create("hover");
hs.properties.fill = am4core.color("#367B25");

// Excluïm Antàrtica del nostre mapa
polygonSeries.exclude = ["AQ"];



// Creem image series
var imageSeries = chart.series.push(new am4maps.MapImageSeries());

// Creem una imathe cercle dins d'un template image series perquè es repliqui a totes les noves imatges
var imageSeriesTemplate = imageSeries.mapImages.template;
var circle = imageSeriesTemplate.createChild(am4core.Circle);
circle.radius = 8;
circle.fill = am4core.color("#B27799");
circle.stroke = am4core.color("#FFFFFF");
circle.strokeWidth = 2;
circle.nonScaling = true;
clickable: true;
circle.tooltipText = "{title}";

// Creem la funció onClick (en aquest cas "hit") que cridarà la funció playPause o playStop depenent de si ja s'està reproduïnt l'audio o no
var escoltant=1;

circle.events.on("hit", function(event) {
    var data = event.target.dataItem.dataContext;
   
  if(escoltant==1){
    x=new Audio(data.radio)
    playPause(x);
    escoltant++;
   }
  else{
    escoltant=1;
    playStop(x);
  }
 
  debugger;
});
// Set property fields
imageSeriesTemplate.propertyFields.latitude = "latitude";
imageSeriesTemplate.propertyFields.longitude = "longitude";

// Informació de les ciutats que volem que es marquin al nostre mapa
imageSeries.data = [{
    "id":1,
    "latitude": 48.856614,
    "longitude": 2.352222,
    "title": "Paris",
    "radio":"http://direct.franceinter.fr/live/franceinter-midfi.mp3"
  }, {
    "id":2,
    "latitude": 40.712775,
    "longitude": -74.005973,
    "title": "New York",
    "radio":"http://bbcwssc.ic.llnwd.net/stream/bbcwssc_mp1_ws-eieuk"
  }, {
    "id":3,
    "latitude": 49.282729,
    "longitude": -123.120738,
    "title": "Vancouver",
    "radio":"http://18843.live.streamtheworld.com/CFRBAM.mp3",
    "informacio": {
      "pais": "Canadà",
      "nomRadio": "Radio Vancouver",
      "informacioRadio": "La CBC Radio One es la radio de noticias e informaciones, que transmite en inglés, perteneciente a la Corporación Canadiense "
      + "de Radioifusión (CBC, del acrónimo en inglés CBC Logo 1992-Present.svg Canadian Broadcasting Corporation). Ofrece programación tanto a nivel local y nacional, y está disponible en las frecuencias AM y FM para el 98 por ciento de los canadienses. "
  }}, {
      "id": 4,
      "latitude": 41.390205, 
      "longitude": 2.154007,
      "title": "Barcelona",
      "radio":"http://catinform.ccma.audioemotion.stream.flumotion.com/ccma/catinform.mp3"
    }, {
      "id": 5,
      "latitude": -37.840935, 
      "longitude": 144.946457,
      "title": "Melbourne",
      "radio":"https://stream.fbiradio.com/stream.mp3"
    }, {
      "id": 6,
      "latitude": 35.652832, 
      "longitude": 139.839478,
      "title": "Tokyo",
      "radio":"http://touhouradio.com:8000/.mp3"
    }, {
      "id": 7,
      "latitude": -23.533773, 
      "longitude": -46.625290,
      "title": "São Paulo",
      "radio":"http://ice1.crossradio.com.br:8526/live.mp3"
    }, {
      "id": 8,
      "latitude": -26.195246, 
      "longitude": 28.034088,
      "title": "Johannesburg",
      "radio":"http://zas5.ndx.co.za:9618/stream/1/"
    }
  ];

var audios = [];



// Afegim la opció de fer zoom al nostre mapa
chart.zoomControl = new am4maps.ZoomControl();

// Afegim i configurem small map
chart.smallMap = new am4maps.SmallMap();
chart.smallMap.series.push(polygonSeries);


// Funcions playPause i playStop, per començar a reproduïr o pausar els audios
function playPause(audio) { 
  audio.play();      
} 

function playStop(audio) {  
  audio.pause();      
} 

/*circle.events.on("over", function(ev) {
  var data2 = ev.target.dataItem.dataContext;
  //var info = document.getElementById("info");
  var nomPais = document.getElementById("paisNom");
  var nomRadio = document.getElementById("radioNom");
  var infoExtra = document.getElementById("extraInfo");

infoPais.innerHTML = "<h3>" + data2.informacio.pais + "</h3>";
  if (data2.informacio.pais) {
    nomPais.innerHTML +=  data2.informacio.pais;
  }

  else {
    nomPais.innerHTML += "<i>Encara no hi ha descripció.</i>"
  }

  if (data2.informacio.nomRadio) {
    nomRadio.innerHTML +=  data2.informacio.nomRadio;
  }

  else {
    nomRadio.innerHTML += "<i>Encara no hi ha descripció.</i>"
  }

  if (data2.informacio.infoExtra) {
    infoExtra.innerHTML +=  data2.informacio.infoExtra;
  }

  else {
    infoExtra.innerHTML += "<i>Encara no hi ha descripció.</i>"
  }
});*/

