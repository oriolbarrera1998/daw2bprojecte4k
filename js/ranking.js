/**
 * Traer datos y crear página.
 */

var userSelection = document.getElementsByClassName('option');

for(let i = 0; i < userSelection.length; i++) {
  userSelection[i].addEventListener("click", function() {
    if (!userSelection[i].className.includes('highlighted')) {
      userSelection[i].className = "option highlighted";
  }
    for(let j = 0; j < userSelection.length; j++) {
      if (j != i) {
        userSelection[j].className = "option";
    }
    }
  })
}